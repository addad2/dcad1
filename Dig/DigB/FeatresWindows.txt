
FeatureName                                          
-----------                                          
Server-Core                                          
NetFx4ServerFeatures                                 
NetFx4                                               
MicrosoftWindowsPowerShellRoot                       
MicrosoftWindowsPowerShell                           
KeyDistributionService-PSH-Cmdlets                   
TlsSessionTicketKey-PSH-Cmdlets                      
Tpm-PSH-Cmdlets                                      
MicrosoftWindowsPowerShellV2                         
Server-Psh-Cmdlets                                   
MicrosoftWindowsPowerShellISE                        
ActiveDirectory-PowerShell                           
DirectoryServices-DomainController                   
DirectoryServices-AdministrativeCenter               
WCF-Services45                                       
WCF-TCP-PortSharing45                                
ServerCore-WOW64                                     
Printing-Client                                      
Printing-Client-Gui                                  
Server-Shell                                         
Server-Gui-Mgmt                                      
RSAT                                                 
DNS-Server-Tools                                     
RSAT-AD-Tools-Feature                                
RSAT-ADDS-Tools-Feature                              
DirectoryServices-DomainController-Tools             
WindowsServerBackupSnapin                            
BitLocker                                            
Internet-Explorer-Optional-amd64                     
MediaPlayback                                        
WindowsMediaPlayer                                   
Printing-XPSServices-Features                        
ServerManager-Core-RSAT                              
ServerManager-Core-RSAT-Role-Tools                   
SmbDirect                                            
Windows-Defender                                     
EnhancedStorage                                      
Microsoft-Windows-GroupPolicy-ServerAdminTools-Update
DNS-Server-Full-Role                                 
Printing-PrintToPDFServices-Features                 
Server-Drivers-General                               
Server-Drivers-Printers                              
Xps-Foundation-Xps-Viewer                            
SearchEngine-Client-Package                          
FileAndStorage-Services                              
Storage-Services                                     
File-Services                                        
CoreFileServer                                       
ServerCore-Drivers-General                           
ServerCore-Drivers-General-WOW64                     
ServerCoreFonts-NonCritical-Fonts-MinConsoleFonts    
ServerCoreFonts-NonCritical-Fonts-BitmapFonts        
ServerCoreFonts-NonCritical-Fonts-TrueType           
ServerCoreFonts-NonCritical-Fonts-UAPFonts           
ServerCoreFonts-NonCritical-Fonts-Support            
SystemDataArchiver                                   



DisplayName                                   
-----------                                   
Active Directory Domain Services              
DNS Server                                    
File and Storage Services                     
File and iSCSI Services                       
File Server                                   
Storage Services                              
.NET Framework 4.7 Features                   
.NET Framework 4.7                            
WCF Services                                  
TCP Port Sharing                              
BitLocker Drive Encryption                    
Enhanced Storage                              
Group Policy Management                       
Remote Server Administration Tools            
Role Administration Tools                     
AD DS and AD LDS Tools                        
Active Directory module for Windows PowerShell
AD DS Tools                                   
Active Directory Administrative Center        
AD DS Snap-Ins and Command-Line Tools         
DNS Server Tools                              
System Data Archiver                          
Windows Defender Antivirus                    
Windows PowerShell                            
Windows PowerShell 5.1                        
Windows PowerShell ISE                        
WoW64 Support                                 
XPS Viewer                                    


Name                                        

ADAuditPlusAgent                            

Quest Change Auditor 7.1.1 Agent            

Netwrix Auditor User Activity Core Service  

ManageEngine EventLogAnalyzer Agent         




Name                                        PackageFullName                                                            
----                                        ---------------                                                            
1527c705-839a-4832-9118-54d4Bd6a0c89        1527c705-839a-4832-9118-54d4Bd6a0c89_10.0.17763.1_neutral_neutral_cw5n1h...
c5e2524a-ea46-4f67-841f-6a9465d9d515        c5e2524a-ea46-4f67-841f-6a9465d9d515_10.0.17763.1_neutral_neutral_cw5n1h...
E2A4F912-2574-4A75-9BB0-0D023378592B        E2A4F912-2574-4A75-9BB0-0D023378592B_10.0.17763.1_neutral_neutral_cw5n1h...
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE        F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE_10.0.17763.1_neutral_neutral_cw5n1h...
InputApp                                    InputApp_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy                      
Microsoft.AAD.BrokerPlugin                  Microsoft.AAD.BrokerPlugin_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy    
Microsoft.AccountsControl                   Microsoft.AccountsControl_10.0.17763.1_neutral__cw5n1h2txyewy              
Microsoft.AsyncTextService                  Microsoft.AsyncTextService_10.0.17763.1_neutral__8wekyb3d8bbwe             
Microsoft.BioEnrollment                     Microsoft.BioEnrollment_10.0.17763.1_neutral__cw5n1h2txyewy                
Microsoft.CredDialogHost                    Microsoft.CredDialogHost_10.0.17763.1_neutral__cw5n1h2txyewy               
Microsoft.ECApp                             Microsoft.ECApp_10.0.17763.1_neutral__8wekyb3d8bbwe                        
Microsoft.LockApp                           Microsoft.LockApp_10.0.17763.1_neutral__cw5n1h2txyewy                      
Microsoft.Win32WebViewHost                  Microsoft.Win32WebViewHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy      
Microsoft.Windows.Apprep.ChxApp             Microsoft.Windows.Apprep.ChxApp_1000.17763.1.0_neutral_neutral_cw5n1h2tx...
Microsoft.Windows.CapturePicker             Microsoft.Windows.CapturePicker_10.0.17763.1_neutral__cw5n1h2txyewy        
Microsoft.Windows.CloudExperienceHost       Microsoft.Windows.CloudExperienceHost_10.0.17763.1_neutral_neutral_cw5n1...
Microsoft.Windows.Cortana                   Microsoft.Windows.Cortana_1.11.6.17763_neutral_neutral_cw5n1h2txyewy       
Microsoft.Windows.NarratorQuickStart        Microsoft.Windows.NarratorQuickStart_10.0.17763.1_neutral_neutral_8wekyb...
Microsoft.Windows.OOBENetworkCaptivePortal  Microsoft.Windows.OOBENetworkCaptivePortal_10.0.17763.1_neutral__cw5n1h2...
Microsoft.Windows.OOBENetworkConnectionFlow Microsoft.Windows.OOBENetworkConnectionFlow_10.0.17763.1_neutral__cw5n1h...
Microsoft.Windows.PeopleExperienceHost      Microsoft.Windows.PeopleExperienceHost_10.0.17763.1_neutral_neutral_cw5n...
Microsoft.Windows.PinningConfirmationDialog Microsoft.Windows.PinningConfirmationDialog_1000.17763.1.0_neutral__cw5n...
Microsoft.Windows.SecHealthUI               Microsoft.Windows.SecHealthUI_10.0.17763.1_neutral__cw5n1h2txyewy          
Microsoft.Windows.ShellExperienceHost       Microsoft.Windows.ShellExperienceHost_10.0.17763.1_neutral_neutral_cw5n1...
Microsoft.Windows.XGpuEjectDialog           Microsoft.Windows.XGpuEjectDialog_10.0.17763.1_neutral_neutral_cw5n1h2tx...
Windows.CBSPreview                          Windows.CBSPreview_10.0.17763.1_neutral_neutral_cw5n1h2txyewy              
windows.immersivecontrolpanel               windows.immersivecontrolpanel_10.0.2.1000_neutral_neutral_cw5n1h2txyewy    
Windows.PrintDialog                         Windows.PrintDialog_6.2.1.0_neutral_neutral_cw5n1h2txyewy                  



FeatureName                                          
-----------                                          
Server-Core                                          
NetFx4ServerFeatures                                 
NetFx4                                               
MicrosoftWindowsPowerShellRoot                       
MicrosoftWindowsPowerShell                           
KeyDistributionService-PSH-Cmdlets                   
TlsSessionTicketKey-PSH-Cmdlets                      
Tpm-PSH-Cmdlets                                      
MicrosoftWindowsPowerShellV2                         
Server-Psh-Cmdlets                                   
MicrosoftWindowsPowerShellISE                        
ActiveDirectory-PowerShell                           
DirectoryServices-DomainController                   
DirectoryServices-AdministrativeCenter               
WCF-Services45                                       
WCF-TCP-PortSharing45                                
ServerCore-WOW64                                     
Printing-Client                                      
Printing-Client-Gui                                  
Server-Shell                                         
Server-Gui-Mgmt                                      
RSAT                                                 
DNS-Server-Tools                                     
RSAT-AD-Tools-Feature                                
RSAT-ADDS-Tools-Feature                              
DirectoryServices-DomainController-Tools             
WindowsServerBackupSnapin                            
BitLocker                                            
Internet-Explorer-Optional-amd64                     
MediaPlayback                                        
WindowsMediaPlayer                                   
Printing-XPSServices-Features                        
ServerManager-Core-RSAT                              
ServerManager-Core-RSAT-Role-Tools                   
SmbDirect                                            
Windows-Defender                                     
EnhancedStorage                                      
Microsoft-Windows-GroupPolicy-ServerAdminTools-Update
DNS-Server-Full-Role                                 
Printing-PrintToPDFServices-Features                 
Server-Drivers-General                               
Server-Drivers-Printers                              
Xps-Foundation-Xps-Viewer                            
SearchEngine-Client-Package                          
FileAndStorage-Services                              
Storage-Services                                     
File-Services                                        
CoreFileServer                                       
ServerCore-Drivers-General                           
ServerCore-Drivers-General-WOW64                     
ServerCoreFonts-NonCritical-Fonts-MinConsoleFonts    
ServerCoreFonts-NonCritical-Fonts-BitmapFonts        
ServerCoreFonts-NonCritical-Fonts-TrueType           
ServerCoreFonts-NonCritical-Fonts-UAPFonts           
ServerCoreFonts-NonCritical-Fonts-Support            
SystemDataArchiver                                   



DisplayName                                   
-----------                                   
Active Directory Domain Services              
DNS Server                                    
File and Storage Services                     
File and iSCSI Services                       
File Server                                   
Storage Services                              
.NET Framework 4.7 Features                   
.NET Framework 4.7                            
WCF Services                                  
TCP Port Sharing                              
BitLocker Drive Encryption                    
Enhanced Storage                              
Group Policy Management                       
Remote Server Administration Tools            
Role Administration Tools                     
AD DS and AD LDS Tools                        
Active Directory module for Windows PowerShell
AD DS Tools                                   
Active Directory Administrative Center        
AD DS Snap-Ins and Command-Line Tools         
DNS Server Tools                              
System Data Archiver                          
Windows Defender Antivirus                    
Windows PowerShell                            
Windows PowerShell 5.1                        
Windows PowerShell ISE                        
WoW64 Support                                 
XPS Viewer                                    


Name                                        

ADAuditPlusAgent                            

Quest Change Auditor 7.1.1 Agent            

Netwrix Auditor User Activity Core Service  

ManageEngine EventLogAnalyzer Agent         




Name                                        PackageFullName                                                                  
----                                        ---------------                                                                  
1527c705-839a-4832-9118-54d4Bd6a0c89        1527c705-839a-4832-9118-54d4Bd6a0c89_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
c5e2524a-ea46-4f67-841f-6a9465d9d515        c5e2524a-ea46-4f67-841f-6a9465d9d515_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
E2A4F912-2574-4A75-9BB0-0D023378592B        E2A4F912-2574-4A75-9BB0-0D023378592B_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE        F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
InputApp                                    InputApp_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy                            
Microsoft.AAD.BrokerPlugin                  Microsoft.AAD.BrokerPlugin_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy          
Microsoft.AccountsControl                   Microsoft.AccountsControl_10.0.17763.1_neutral__cw5n1h2txyewy                    
Microsoft.AsyncTextService                  Microsoft.AsyncTextService_10.0.17763.1_neutral__8wekyb3d8bbwe                   
Microsoft.BioEnrollment                     Microsoft.BioEnrollment_10.0.17763.1_neutral__cw5n1h2txyewy                      
Microsoft.CredDialogHost                    Microsoft.CredDialogHost_10.0.17763.1_neutral__cw5n1h2txyewy                     
Microsoft.ECApp                             Microsoft.ECApp_10.0.17763.1_neutral__8wekyb3d8bbwe                              
Microsoft.LockApp                           Microsoft.LockApp_10.0.17763.1_neutral__cw5n1h2txyewy                            
Microsoft.Win32WebViewHost                  Microsoft.Win32WebViewHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy            
Microsoft.Windows.Apprep.ChxApp             Microsoft.Windows.Apprep.ChxApp_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy     
Microsoft.Windows.CapturePicker             Microsoft.Windows.CapturePicker_10.0.17763.1_neutral__cw5n1h2txyewy              
Microsoft.Windows.CloudExperienceHost       Microsoft.Windows.CloudExperienceHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy 
Microsoft.Windows.Cortana                   Microsoft.Windows.Cortana_1.11.6.17763_neutral_neutral_cw5n1h2txyewy             
Microsoft.Windows.NarratorQuickStart        Microsoft.Windows.NarratorQuickStart_10.0.17763.1_neutral_neutral_8wekyb3d8bbwe  
Microsoft.Windows.OOBENetworkCaptivePortal  Microsoft.Windows.OOBENetworkCaptivePortal_10.0.17763.1_neutral__cw5n1h2txyewy   
Microsoft.Windows.OOBENetworkConnectionFlow Microsoft.Windows.OOBENetworkConnectionFlow_10.0.17763.1_neutral__cw5n1h2txyewy  
Microsoft.Windows.PeopleExperienceHost      Microsoft.Windows.PeopleExperienceHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy
Microsoft.Windows.PinningConfirmationDialog Microsoft.Windows.PinningConfirmationDialog_1000.17763.1.0_neutral__cw5n1h2txyewy
Microsoft.Windows.SecHealthUI               Microsoft.Windows.SecHealthUI_10.0.17763.1_neutral__cw5n1h2txyewy                
Microsoft.Windows.ShellExperienceHost       Microsoft.Windows.ShellExperienceHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy 
Microsoft.Windows.XGpuEjectDialog           Microsoft.Windows.XGpuEjectDialog_10.0.17763.1_neutral_neutral_cw5n1h2txyewy     
Windows.CBSPreview                          Windows.CBSPreview_10.0.17763.1_neutral_neutral_cw5n1h2txyewy                    
windows.immersivecontrolpanel               windows.immersivecontrolpanel_10.0.2.1000_neutral_neutral_cw5n1h2txyewy          
Windows.PrintDialog                         Windows.PrintDialog_6.2.1.0_neutral_neutral_cw5n1h2txyewy                        


